package studio.phillip.foodmydoc.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import studio.phillip.foodmydoc.data.models.Recipe
import studio.phillip.foodmydoc.utils.Contants


class RecipeRecyclerAdapter(
    private val mOnRecipeListener: OnRecipeListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mRecipes: ArrayList<Recipe>

    init {
        mRecipes = ArrayList()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        when (i) {
            LOADING_TYPE -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(
                        studio.phillip.foodmydoc.R.layout.layout_loading_list_item,
                        viewGroup,
                        false
                    )
                return LoadingViewHolder(view)
            }
            EXHAUSTED_TYPE -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(
                        studio.phillip.foodmydoc.R.layout.layout_search_exhausted,
                        viewGroup,
                        false
                    )
                return SearchExhaustedViewHolder(view)
            }
            CATEGORY_TYPE -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(
                        studio.phillip.foodmydoc.R.layout.layout_category_list_item,
                        viewGroup,
                        false
                    )
                return CategoryViewHolder(view, mOnRecipeListener)
            }
            RECIPE_TYPE -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(
                        studio.phillip.foodmydoc.R.layout.layout_recipe_list_item,
                        viewGroup,
                        false
                    )
                return RecipeViewHolder(view, mOnRecipeListener)

            }
            else -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(
                        studio.phillip.foodmydoc.R.layout.layout_recipe_list_item,
                        viewGroup,
                        false
                    )
                return RecipeViewHolder(view, mOnRecipeListener)
            }
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        when (getItemViewType(i)) {
            RECIPE_TYPE -> {
                // set the image
                val options = RequestOptions()
                    .centerCrop()
                    .error(studio.phillip.foodmydoc.R.drawable.ic_launcher_background)

                Glide.with(viewHolder.itemView)
                    .setDefaultRequestOptions(options)
                    .load(mRecipes[i].image_url)
                    .into((viewHolder as RecipeViewHolder).image)

                (viewHolder).title.text = mRecipes!![i].title
                viewHolder.publisher.text = mRecipes!![i].publisher
                viewHolder.socialScore.text = Math.round(mRecipes!![i].social_rank).toString()
            }
            CATEGORY_TYPE -> {
                // set the image
                val options = RequestOptions()
                    .centerCrop()
                    .error(studio.phillip.foodmydoc.R.drawable.ic_launcher_background)
                Glide.with(viewHolder.itemView)
                    .setDefaultRequestOptions(options)
                    .load(mRecipes[i].image_url!!.toInt())
                    .into((viewHolder as CategoryViewHolder).categoryImage)

                viewHolder.categoryTitle.text = mRecipes[i].title
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (mRecipes.get(position).social_rank == (-1).toFloat()) {
            return CATEGORY_TYPE;
        } else if (mRecipes.get(position).title.equals("LOADING...")
        ) {
            return LOADING_TYPE
        } else if (position == mRecipes.size - 1
            && position != 0
            && !mRecipes.get(position).title.equals("EXHAUSTED...")
        ) {
            return LOADING_TYPE;
        } else if (mRecipes.get(position).title.equals("EXHAUSTED...")) {
            return EXHAUSTED_TYPE
        } else {
            return RECIPE_TYPE
        }
    }

    override fun getItemCount(): Int {
        if (mRecipes != null) {
            return mRecipes.size
        }
        return 0
    }

    fun setQueryExhausted() {
        hideLoading()
        val exhaustedRecipe = Recipe()
        exhaustedRecipe.title = "EXHAUSTED..."
        mRecipes.add(exhaustedRecipe)
        notifyDataSetChanged()
    }

    fun hideLoading() {
        if (isLoading()) {
            for (recipe in mRecipes) {
                if (recipe.title == "LOADING...") {
                    mRecipes.remove(recipe)
                }
            }
            notifyDataSetChanged()
        }
    }

    fun displayLoading() {
        if (!isLoading()) {
            val recipe = Recipe()
            recipe.title = "LOADING..."
            val loadingList = ArrayList<Recipe>()
            loadingList.add(recipe)
            mRecipes = loadingList
            notifyDataSetChanged()
        }
    }

    fun displaySearchCategories() {
        val categories = ArrayList<Recipe>()
        for (i in 0 until Contants.DEFAULT_SEARCH_CATEGORIES.size) {
            val recipe = Recipe()
            recipe.title = Contants.DEFAULT_SEARCH_CATEGORIES[i]
            recipe.image_url = Contants.DEFAULT_SEARCH_CATEGORY_IMAGES[i].toString()
            recipe.social_rank = (-1).toFloat()
            categories.add(recipe)
        }
        mRecipes = categories
        notifyDataSetChanged()
    }

    private fun isLoading(): Boolean {
        if (mRecipes.size > 0) {
            if (mRecipes[mRecipes.size - 1].title.equals("LOADING...")) {
                return true
            }
        }
        return false
    }

    fun setRecipes(recipes: ArrayList<Recipe>) {
        mRecipes = recipes
        notifyDataSetChanged()
    }

    fun getSelectedRecipe(position: Int): Recipe? {
        if (mRecipes != null) {
            if (mRecipes.size > 0) {
                return mRecipes[position]
            }
        }
        return null
    }

    companion object {
        private val RECIPE_TYPE = 1
        private val LOADING_TYPE = 2
        private val CATEGORY_TYPE = 3
        private val EXHAUSTED_TYPE = 4
    }

}