package studio.phillip.foodmydoc.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import studio.phillip.foodmydoc.R

class RecipeViewHolder(itemView: View, var onRecipeListener: OnRecipeListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener {

    var title: TextView
    var publisher: TextView
    var socialScore: TextView
    var image: ImageView

    init {

        title = itemView.findViewById(R.id.recipe_title)
        publisher = itemView.findViewById(R.id.recipe_publisher)
        socialScore = itemView.findViewById(R.id.recipe_social_score)
        image = itemView.findViewById(R.id.recipe_image)

        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        onRecipeListener.onRecipeClick(adapterPosition)
    }
}