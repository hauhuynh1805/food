package studio.phillip.foodmydoc.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.hdodenhof.circleimageview.CircleImageView
import studio.phillip.foodmydoc.R


class CategoryViewHolder(itemView: View, var onRecipeListener: OnRecipeListener) :
    RecyclerView.ViewHolder(itemView), View.OnClickListener {

    var categoryImage: CircleImageView
    var categoryTitle: TextView
    var listener: OnRecipeListener

    init {
        categoryImage = itemView.findViewById(R.id.category_image);
        categoryTitle = itemView.findViewById(R.id.category_title);
        listener = onRecipeListener;
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        onRecipeListener.onCategoryClick(categoryTitle.text.toString())
    }
}