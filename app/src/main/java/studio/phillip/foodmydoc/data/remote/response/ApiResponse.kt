package studio.phillip.foodmydoc.data.remote.response

import retrofit2.Response
import java.io.IOException

open class ApiResponse<T> {

    fun create(error: Throwable): ApiResponse<T> {
        var errorMessage = ""

        if (error.message.isNullOrEmpty()) {
            errorMessage = "Unknown error Check network connection"
        } else {
            errorMessage = error.message.toString()

        }
        return ApiErrorResponse(errorMessage)
    }

    fun create(response: Response<T>): ApiResponse<T> {
        if (response.isSuccessful) {
            val body = response.body()

            return if (body == null || response.code() == 204) { // 204 is empty response
                ApiEmptyResponse()
            } else {
                ApiSuccessResponse(body)
            }
        } else {
            var errorMsg = ""
            try {
                errorMsg = response.errorBody()!!.string()
            } catch (e: IOException) {
                e.printStackTrace()
                errorMsg = response.message()
            }

            return ApiErrorResponse(errorMsg)
        }
    }


    class ApiSuccessResponse<T> constructor(val body: T) : ApiResponse<T>()
    class ApiErrorResponse<T> constructor(val errorMessage: String) : ApiResponse<T>()
    class ApiEmptyResponse<T> : ApiResponse<T>()

}
