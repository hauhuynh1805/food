package studio.phillip.foodmydoc.data.remote.response

import com.google.gson.annotations.SerializedName
import studio.phillip.foodmydoc.data.models.Recipe

data class RecipeSearchResponse(
    @SerializedName("count")
    val count: Int,
    @SerializedName("recipes")
    val recipes: List<Recipe>
)