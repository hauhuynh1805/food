package studio.phillip.foodmydoc.data.remote.response

import com.google.gson.annotations.SerializedName
import studio.phillip.foodmydoc.data.models.Recipe

data class RecipeResponse(
    @SerializedName("recipe")
    val recipe: Recipe
)