package studio.phillip.foodmydoc.data.models

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "recipes")
class Recipe : Parcelable {
    @PrimaryKey
    @NonNull
    var recipe_id: String? = null

    @ColumnInfo(name = "title")
    var title: String? = null

    @ColumnInfo(name = "publisher")
    var publisher: String? = null

    @ColumnInfo(name = "image_url")
    var image_url: String? = null

    @ColumnInfo(name = "ingredients")
    var ingredients: Array<String>? = null

    @ColumnInfo(name = "social_rank")
    var social_rank: Float = 0.toFloat()

    /**
     * Saves current timestamp in **SECONDS**
     */
    @ColumnInfo(name = "timestamp")
    var timestamp: Int = 0

    constructor(
        title: String, publisher: String, ingredients: Array<String>, recipe_id: String,
        image_url: String, social_rank: Float, timestamp: Int
    ) {
        this.title = title
        this.publisher = publisher
        this.ingredients = ingredients
        this.recipe_id = recipe_id
        this.image_url = image_url
        this.social_rank = social_rank
        this.timestamp = timestamp
    }

    protected constructor(`in`: Parcel) {
        title = `in`.readString()
        publisher = `in`.readString()
        ingredients = `in`.createStringArray()
        recipe_id = `in`.readString()
        image_url = `in`.readString()
        social_rank = `in`.readFloat()
        timestamp = `in`.readInt()
    }

    constructor() {
    }

    override fun toString(): String {
        return "Recipe{" +
                "title='" + title + '\''.toString() +
                ", publisher='" + publisher + '\''.toString() +
                ", ingredients=" + Arrays.toString(ingredients) +
                ", recipe_id='" + recipe_id + '\''.toString() +
                ", image_url='" + image_url + '\''.toString() +
                ", social_rank=" + social_rank +
                ", timestamp=" + timestamp +
                '}'.toString()
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(title)
        dest.writeString(publisher)
        dest.writeStringArray(ingredients)
        dest.writeString(recipe_id)
        dest.writeString(image_url)
        dest.writeFloat(social_rank)
        dest.writeInt(timestamp)
    }

    companion object CREATOR : Parcelable.Creator<Recipe> {
        override fun createFromParcel(parcel: Parcel): Recipe {
            return Recipe(parcel)
        }

        override fun newArray(size: Int): Array<Recipe?> {
            return arrayOfNulls(size)
        }
    }
}