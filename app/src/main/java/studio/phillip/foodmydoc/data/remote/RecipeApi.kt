package studio.phillip.foodmydoc.data.remote

import androidx.lifecycle.LiveData
import retrofit2.http.GET
import retrofit2.http.Query
import studio.phillip.foodmydoc.data.remote.response.ApiResponse
import studio.phillip.foodmydoc.data.remote.response.RecipeResponse
import studio.phillip.foodmydoc.data.remote.response.RecipeSearchResponse


interface RecipeApi {

    @GET("api/get")
    fun getRecipe(
        @Query("key") key: String,
        @Query("rId") rId: String
    ): LiveData<ApiResponse<RecipeResponse>>

    @GET("api/search")
    fun searchRecipe(
        @Query("key") key: String,
        @Query("q") query: String,
        @Query("page") page: String
    ): LiveData<ApiResponse<RecipeSearchResponse>>

}