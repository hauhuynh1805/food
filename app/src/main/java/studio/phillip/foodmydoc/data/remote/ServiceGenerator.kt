package studio.phillip.foodmydoc.data.remote

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import studio.phillip.foodmydoc.utils.Contants
import studio.phillip.foodmydoc.utils.LiveDataCallAdapterFactory


class ServiceGenerator {
    companion object {
        private val retrofitBuilder = Retrofit.Builder()
            .baseUrl(Contants.BASE_URL)
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
        private val retrofit = retrofitBuilder.build()
        private val receiptAPI = retrofit.create(RecipeApi::class.java)

        fun getRecipeApi(): RecipeApi {
            return receiptAPI
        }
    }
}