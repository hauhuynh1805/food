package studio.phillip.foodmydoc.repositories

import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.lifecycle.LiveData
import studio.phillip.foodmydoc.AppExecutors
import studio.phillip.foodmydoc.data.local.RecipeDao
import studio.phillip.foodmydoc.data.local.RecipeDatabase
import studio.phillip.foodmydoc.data.models.Recipe
import studio.phillip.foodmydoc.data.remote.ServiceGenerator
import studio.phillip.foodmydoc.data.remote.response.ApiResponse
import studio.phillip.foodmydoc.data.remote.response.RecipeSearchResponse
import studio.phillip.foodmydoc.utils.Contants
import studio.phillip.foodmydoc.utils.NetworkBoundResource
import studio.phillip.foodmydoc.utils.Resource


class RecipeRepository {
    private var recipeDao: RecipeDao


    constructor(mContext: Context) {
        recipeDao = RecipeDatabase.getInstance(mContext).recipeDao()
    }

    companion object {
        private var instance: RecipeRepository? = null
        fun getInstance(mContext: Context): RecipeRepository {
            if (instance == null) {
                instance = RecipeRepository(mContext)
            }
            return instance as RecipeRepository
        }
    }

    fun searchRecipesApi(query: String, pageNumber: Int): LiveData<Resource<List<Recipe>>> {
        return object :
            NetworkBoundResource<List<Recipe>, RecipeSearchResponse>(AppExecutors.getInstance()) {

            public override fun saveCallResult(@NonNull item: RecipeSearchResponse) {
                if (item.recipes != null) { // recipe list will be null if api key is expired
                    var recipes = ArrayList<Recipe>()
                    recipes.addAll(item.recipes)
                    var index = 0
                    for (rowId in recipeDao.insertRecipes(recipes)) {
                        if (rowId == -1.toLong()) { // conflict detected
                            Log.d(
                                "AAA",
                                "saveCallResult: CONFLICT... This recipe is already in cache."
                            )
                            // if already exists, I don't want to set the ingredients or timestamp b/c they will be erased
                            recipeDao.updateRecipe(
                                recipes[index].recipe_id.toString(),
                                recipes[index].title.toString(),
                                recipes[index].publisher.toString(),
                                recipes[index].image_url.toString(),
                                recipes[index].social_rank
                            )
                        }
                        index++
                    }
                }


            }

            public override fun shouldFetch(@Nullable data: List<Recipe>?): Boolean {
                return true // always query the network since the queries can be anything
            }

            @NonNull
            public override fun loadFromDb(): LiveData<List<Recipe>> {
                return recipeDao.searchRecipes(query, pageNumber)
            }

            @NonNull
            public override fun createCall(): LiveData<ApiResponse<RecipeSearchResponse>> {
                return ServiceGenerator.getRecipeApi().searchRecipe(
                    Contants.API_KEY,
                    query,
                    pageNumber.toString()
                )

            }

        }.asLiveData
    }
}