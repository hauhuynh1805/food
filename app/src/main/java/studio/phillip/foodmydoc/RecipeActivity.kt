package studio.phillip.foodmydoc

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import studio.phillip.foodmydoc.data.models.Recipe
import studio.phillip.foodmydoc.viewmodels.RecipeViewModel


class RecipeActivity : BaseActivity() {

    // UI components
    private lateinit var mRecipeImage: ImageView
    private lateinit var mRecipeTitle: TextView
    private lateinit var mRecipeRank: TextView
    private lateinit var mRecipeIngredientsContainer: LinearLayout
    private lateinit var mScrollView: ScrollView

    private lateinit var mRecipeViewModel: RecipeViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe)
        mRecipeImage = findViewById(R.id.recipe_image)
        mRecipeTitle = findViewById(R.id.recipe_title)
        mRecipeRank = findViewById(R.id.recipe_social_score)
        mRecipeIngredientsContainer = findViewById(R.id.ingredients_container)
        mScrollView = findViewById(R.id.parent)

        mRecipeViewModel = ViewModelProviders.of(this).get(RecipeViewModel::class.java)

        showProgressBar(true)
        subscribeObservers()
        getIncomingIntent()

    }


    private fun getIncomingIntent() {
        if (intent.hasExtra("recipe")) {
//            val recipe = intent.getParcelableExtra<Parcelable>("recipe") as Recipe
//            Log.d("AAA", "getIncomingIntent: " + recipe.title);
//            mRecipeViewModel.searchRecipeApi(recipe.recipe_id.toString());
        }
    }

    private fun subscribeObservers() {
//        mRecipeViewModel.getRecipe().observe(this, object : Observer<Recipe> {
//            override fun onChanged(@Nullable recipe: Recipe?) {
//                if (recipe != null) {
//                    if (recipe.recipe_id.equals(mRecipeViewModel.getRecipeId())) {
//                        setRecipeProperties(recipe)
//                        mRecipeViewModel.setRetrievedRecipe(true)
//                    }
//                }
//            }
//        })
//
//        mRecipeViewModel.isRecipeRequestTimedOut().observe(this, object : Observer<Boolean> {
//            override fun onChanged(t: Boolean) {
//                Log.d("AAA-t", t.toString())
//                Log.d("AAA-didRetrieveRecipe", mRecipeViewModel.didRetrieveRecipe().toString())
//                if (t && !mRecipeViewModel.didRetrieveRecipe()) {
//                    Log.d("AAA", "onChanged: timed out..")
//                    displayErrorScreen("Error retrieving data. Check network connection.");
//                }
//            }
//
//        })
    }

    private fun setRecipeProperties(recipe: Recipe?) {
        if (recipe != null) {
            val requestOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)

            Glide.with(this)
                .setDefaultRequestOptions(requestOptions)
                .load(recipe.image_url)
                .into(mRecipeImage)

            mRecipeTitle.text = recipe.title
            mRecipeRank.text = Math.round(recipe.social_rank).toString()

            mRecipeIngredientsContainer.removeAllViews()
            for (ingredient in recipe.ingredients!!) {
                val textView = TextView(this)
                textView.text = ingredient
                textView.textSize = 15f
                textView.layoutParams = LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
                )
                mRecipeIngredientsContainer.addView(textView)
            }
        }

        showParent()
        showProgressBar(false)
    }

    private fun displayErrorScreen(errorMessage: String) {
        mRecipeTitle.text = "Error retrieveing recipe..."
        mRecipeRank.text = ""
        val textView = TextView(this)
        if (errorMessage != "") {
            textView.text = errorMessage
        } else {
            textView.text = "Error"
        }
        textView.textSize = 15f
        textView.layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        mRecipeIngredientsContainer.addView(textView)

        val requestOptions = RequestOptions()
            .placeholder(R.drawable.ic_launcher_background)

        Glide.with(this)
            .setDefaultRequestOptions(requestOptions)
            .load(R.drawable.ic_launcher_background)
            .into(mRecipeImage)

        showParent()
        showProgressBar(false)
    }

    private fun showParent() {
        mScrollView.visibility = View.VISIBLE
    }

}