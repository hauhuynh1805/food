package studio.phillip.foodmydoc

import android.os.Handler
import android.os.Looper
import androidx.annotation.NonNull
import java.util.concurrent.Executor
import java.util.concurrent.Executors


class AppExecutors {
    private val mDiskIO = Executors.newSingleThreadExecutor()
    private val mMainThreadExecutor = MainThreadExecutor()

    companion object {
        private var instance: AppExecutors? = null

        fun getInstance(): AppExecutors {
            if (instance == null) {
                instance = AppExecutors()
            }
            return instance as AppExecutors
        }

        private class MainThreadExecutor() : Executor {
            private val mainThreadHandler = Handler(Looper.getMainLooper())
            override fun execute(@NonNull command: Runnable) {
                mainThreadHandler.post(command)
            }

        }
    }

    fun mainThread(): Executor {
        return mMainThreadExecutor
    }

    fun diskIO(): Executor {
        return mDiskIO
    }
}