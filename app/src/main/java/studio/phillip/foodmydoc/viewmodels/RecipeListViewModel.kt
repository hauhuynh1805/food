package studio.phillip.foodmydoc.viewmodels


import android.app.Application
import androidx.annotation.Nullable
import androidx.lifecycle.*
import studio.phillip.foodmydoc.data.models.Recipe
import studio.phillip.foodmydoc.repositories.RecipeRepository
import studio.phillip.foodmydoc.utils.Resource


class RecipeListViewModel constructor(application: Application) : AndroidViewModel(application) {

    private var mViewState: MutableLiveData<ViewState>
    private var recipeRepository: RecipeRepository
    private val recipes = MediatorLiveData<Resource<List<Recipe>>>()

    enum class ViewState {
        CATEGORIES, RECIPES
    }

    init {
        mViewState = MutableLiveData()
        recipeRepository = RecipeRepository.getInstance(application)
        mViewState.value = ViewState.CATEGORIES
    }

    fun getViewState(): LiveData<ViewState> {
        return mViewState
    }

    fun getRecipes(): LiveData<Resource<List<Recipe>>> {
        return recipes
    }

    fun searchRecipesApi(query: String, pageNumber: Int) {
        val repositorySource = recipeRepository.searchRecipesApi(query, pageNumber)
        recipes.addSource(repositorySource, object : Observer<Resource<List<Recipe>>> {
            override fun onChanged(@Nullable listResource: Resource<List<Recipe>>) {
                recipes.setValue(listResource)
            }
        })
    }
}


