package studio.phillip.foodmydoc.utils

import studio.phillip.foodmydoc.R

object Contants {

    val BASE_URL: String = "https://www.food2fork.com"
    val API_KEY: String = "0cacb13bad7b8a17b9933f0494ae158a"
    val NETWORK_TIMEOUT: Long = 3000

    val DEFAULT_SEARCH_CATEGORIES =
        arrayOf("Barbeque", "Breakfast", "Chicken", "Beef", "Brunch", "Dinner", "Wine", "Italian")

    val DEFAULT_SEARCH_CATEGORY_IMAGES =
        arrayOf(
            R.drawable.barbeque,
            R.drawable.breakfast,
            R.drawable.chicken,
            R.drawable.beef,
            R.drawable.brunch,
            R.drawable.dinner,
            R.drawable.wine,
            R.drawable.italian
        )
}