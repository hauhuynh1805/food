package studio.phillip.foodmydoc

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import studio.phillip.foodmydoc.adapter.OnRecipeListener
import studio.phillip.foodmydoc.adapter.RecipeRecyclerAdapter
import studio.phillip.foodmydoc.utils.Resource
import studio.phillip.foodmydoc.utils.Testing
import studio.phillip.foodmydoc.utils.VerticalSpacingItemDecorator
import studio.phillip.foodmydoc.viewmodels.RecipeListViewModel


class RecipeListActivity : BaseActivity(), OnRecipeListener {
    private lateinit var mRecipeListViewModel: RecipeListViewModel

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mAdapter: RecipeRecyclerAdapter
    private lateinit var mSearchView: SearchView
    private lateinit var mToolbar: Toolbar
    private val TAG = "RecipeListActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mRecyclerView = findViewById(R.id.recipe_list);
        mSearchView = findViewById(R.id.search_view)
        mToolbar = findViewById(R.id.toolbar)

        mRecipeListViewModel = ViewModelProviders.of(this).get(RecipeListViewModel::class.java)
        initRecyclerView()
        intiSearchView()
        subscribeObservers()
        setSupportActionBar(mToolbar)

        var resource = Resource(Resource.Status.SUCCESS, "AAA", " BBBBB")
    }

    override fun onRecipeClick(position: Int) {
        val intent = Intent(this, RecipeActivity::class.java)
        intent.putExtra("recipe", mAdapter.getSelectedRecipe(position))
        startActivity(intent)
    }

    override fun onCategoryClick(category: String) {
        searchRecipeApi(category);
    }

    private fun subscribeObservers() {
        mRecipeListViewModel.getRecipes().observe(this,
            Observer { listResource ->
                if (listResource != null) {
                    Log.d(TAG, "onChanged: status: " + listResource.status)

                    if (listResource.data != null) {
                        Testing.printRecipes(listResource.data, "data: ")
                    }
                }
            })

        mRecipeListViewModel.getViewState()
            .observe(this, object : Observer<RecipeListViewModel.ViewState> {
                override fun onChanged(viewState: RecipeListViewModel.ViewState?) {
                    if (viewState != null) {
                        when (viewState) {
                            RecipeListViewModel.ViewState.RECIPES -> {

                            }
                            RecipeListViewModel.ViewState.CATEGORIES -> {
                                displaySearchCategories()
                            }
                        }
                    }
                }
            })
    }

    private fun initRecyclerView() {
        mAdapter = RecipeRecyclerAdapter(this)
        mRecyclerView.setAdapter(mAdapter)
        mRecyclerView.setLayoutManager(LinearLayoutManager(this))
        mRecyclerView.addItemDecoration(VerticalSpacingItemDecorator(3))
    }

    private fun intiSearchView() {
        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                searchRecipeApi(query.toString());
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

        })
    }

    private fun displaySearchCategories() {
        mAdapter.displaySearchCategories();
    }

    private fun searchRecipeApi(query: String) {
        mRecipeListViewModel.searchRecipesApi(query, 1)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_categories -> {
                displaySearchCategories()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.recipe_search_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }
}